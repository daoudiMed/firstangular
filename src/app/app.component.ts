import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  
  ngOnInit() {
    return false;
  }
  
  //title = 'firstAngular';
  private listeNom:string[] =[];
  private nom:string = '';
  private logo:string = './assets/angularLogo.png';

  constructor(){
    }

  createListe(){
    if(this.nom){
      this.listeNom.push(this.nom);
      this.nom = "";
    }
  }
}
